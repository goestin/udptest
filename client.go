package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"sync"
	"syscall"
	"time"
)

type KeyRange []uint64

func (a KeyRange) Len() int           { return len(a) }
func (a KeyRange) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a KeyRange) Less(i, j int) bool { return a[i] < a[j] }

func main() {
	// options
	var host = flag.String("host", "127.0.0.1", "Host to connect to.")
	var port = flag.Int("port", 31337, "Port to connect to.")
	var count = flag.Int("count", 10, "Number of packets to send.")
	var interval = flag.Int("interval", 1000, "Interval in ms.")
	var filter = flag.Int("filter", 0, "Only show RTT greater then filter value.")
	flag.Parse()

	// set up the connection
	ServerAddr, err := net.ResolveUDPAddr("udp", *host+":"+strconv.Itoa(*port))
	if err != nil {
		panic(err)
	}
	Conn, err := net.DialUDP("udp", nil, ServerAddr)
	if err != nil {
		panic(err)
	}
	defer Conn.Close()

	// status for every packet.
	data := make(map[uint64]uint64)

	// set up the communication channels
	c := make(chan os.Signal, 1)
	msgTX := make(chan uint64, 1024)
	msgRX := make(chan uint64, 1024)
	sQuit := make(chan bool)  // sender quit chan TODO: Justin fatsoenlijke quit structuur...
	shQuit := make(chan bool) // sender quit chan TODO: Justin fatsoenlijke quit structuur...
	rQuit := make(chan bool)  // receiver quit chan TODO: Justin fatsoenlijke quit structuur...
	rhQuit := make(chan bool) // receiver quit chan TODO: Justin fatsoenlijke quit structuur...
	var w sync.WaitGroup
	var mutex sync.Mutex

	// ==== GO ROUTINES ====

	// Handle ctrl-c
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go trapThread(c, sQuit, shQuit, rQuit, rhQuit)

	// Handle send traffic
	go sendHandler(msgTX, &mutex, data, &w, shQuit)

	// Handle received traffic
	go receiveHandler(msgRX, &mutex, data, &w, rhQuit)

	// Start the sender thread.
	go sender(&w, msgTX, Conn, interval, count, sQuit)

	// Start the receiver thread.
	go receiver(Conn, msgRX, &w, rQuit)

	time.Sleep(time.Millisecond * 2)

	fmt.Println(">>> Waiting for waitgroup to finish...")
	w.Wait() // wacht totdat alle send packets in de data struct zitten.
	fmt.Println(">>> Waitgroup finished...")
	cleanup(data, filter)
	fmt.Println(">>> Program End <<<")
}

// ==== end of main() ===

// makeTimestampByte returns 8 byte with the unix epoch in milliseconds
func makeTimestampByte() []byte {
	b := make([]byte, 8)
	t := time.Now().UnixNano() / int64(time.Millisecond*1)
	binary.BigEndian.PutUint64(b, uint64(t))
	return b
}

func cleanup(data map[uint64]uint64, filter *int) {
	printData(data, filter)
}

func printData(data map[uint64]uint64, filter *int) {
	counterReply := 0
	counterNoReply := 0

	var keys KeyRange
	var values []uint64
	for k, v := range data {
		keys = append(keys, k)
		values = append(values, v)
	}
	sort.Sort(keys)

	for _, k := range keys {
		if data[k] == 0 {
			fmt.Printf("msgID: %d RTT: <NO-REPLY>\n", k)
			counterNoReply++
		} else {
			rtt := data[k] - k
			if rtt >= uint64(*filter) {
				fmt.Printf("msgID: %d RTT: %d |", k, rtt)
				for i := uint64(0); i < rtt; i++ {
					fmt.Print("=")
				}
				fmt.Println("|")
			}
			counterReply++
		}
	}

	fmt.Println("=======================")
	fmt.Printf("Total: %d Responses: %d No Responses: %d\n", len(data), counterReply, counterNoReply)
}

// Handle ctrl-c
func trapThread(c chan os.Signal, shq chan bool, rhQuit chan bool, sq chan bool, rq chan bool) {
	fmt.Println(">>> Starting trapThread...")
	<-c
	fmt.Println(">>> Caught: CTRL-C")
	sq <- true
	shq <- true
	rq <- true
	rhQuit <- true
}

// Handle send traffic
func sendHandler(c chan uint64,
	mutex *sync.Mutex,
	data map[uint64]uint64,
	w *sync.WaitGroup,
	quit chan bool) {
	fmt.Println(">>> Starting sendHandler thread...")
	w.Add(1)
	for msg := range c {
		mutex.Lock()
		data[msg] = 0
		mutex.Unlock()
		fmt.Printf(".")
		select {
		case <-quit:
			fmt.Println(">>> sendHandler: got quit signal.")
			w.Done()
			return
		default:
			continue
		}
	}
	w.Done()
	fmt.Println(">>> Stopping sendHandler thread...")
}

// Handle receive traffic
func receiveHandler(c chan uint64,
	mutex *sync.Mutex,
	data map[uint64]uint64,
	w *sync.WaitGroup,
	quit chan bool) {
	fmt.Println(">>> Starting receiveHandler thread...")
	w.Add(1)
	for msg := range c {
		t := binary.BigEndian.Uint64(makeTimestampByte())
		mutex.Lock()
		data[msg] = t
		mutex.Unlock()
		select {
		case <-quit:
			fmt.Print(">>> receiveHandler: ctrl-c")
			w.Done()
			return
		default:
			continue
		}
	}
	fmt.Println(">>> receiveHandler: channel closed")
	w.Done()
	fmt.Println(">>> Stopping receiveHandler thread...")
}

func sender(w *sync.WaitGroup, msgTX chan uint64, c *net.UDPConn, interval *int, count *int, q chan bool) {
	fmt.Println(">>> Starting sender thread.")
	time.Sleep(time.Millisecond * 1) // The receiver thread moet ook gestart zijn natuurlijk.
	// add a lock to the waitgroup.
	// to indicate that the sender thread is running.
	// when we quit, we need to wait for this thread to finish.
	w.Add(1)

	ktnxbye := make([]byte, 8)
	binary.BigEndian.PutUint64(ktnxbye, uint64(0))
	// Aight, lets fire some packets!
	for i := 0; i < *count; i++ {
		time.Sleep(time.Millisecond * time.Duration(int64(*interval)))
		t := makeTimestampByte()
		// send the msgID of the msg to send
		// to the msgTX channel
		msgID := binary.BigEndian.Uint64(t)
		msgTX <- msgID

		// Send the timestamp to the server
		_, err := c.Write(t)
		if err != nil {
			panic(err)
		}
		select {
		case <-q:
			c.Write(ktnxbye)
			close(msgTX)
			w.Done()
			return
		default:
			continue

		}
	}
	c.Write(ktnxbye)
	close(msgTX)
	w.Done()
	fmt.Println(">>> Stopping sender thread.")
}

func receiver(c *net.UDPConn, msgRX chan uint64, w *sync.WaitGroup, q chan bool) {
	fmt.Println(">>> Starting receiver thread.")
	w.Add(1)
	for {
		r := make([]byte, 8)
		_, err := c.Read(r)
		if err != nil {
			panic(err)
		}
		msgID := binary.BigEndian.Uint64(r)
		if msgID == 0 {
			// special message to signal end of transmission
			// BUG: this will fail if the packet is lost!
			fmt.Println(">>> Received: received EOF packet")
			break
		}
		msgRX <- msgID
		select {
		case <-q:
			close(msgRX)
			w.Done()
			return
		default:
			continue
		}
	}
	close(msgRX)
	w.Done()
	fmt.Println("Stopping receiver thread.")
}
