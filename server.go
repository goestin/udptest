package main

import (
	"encoding/binary"
	"flag"
	"fmt"
	"net"
	"strconv"
)

func main() {
	var host = flag.String("host", "127.0.0.1", "Host ip to bind on.")
	var port = flag.Int("port", 31337, "Udp port to bind on.")
	flag.Parse()

	fmt.Printf("The port is: %d\n", *port)

	ServerAddr, err := net.ResolveUDPAddr("udp", *host+":"+strconv.Itoa(*port))
	if err != nil {
		panic(err)
	}

	ServerConn, err := net.ListenUDP("udp", ServerAddr)
	if err != nil {
		panic(err)
	}
	defer ServerConn.Close()
	fmt.Println("Listening on: :" + strconv.Itoa(*port))

	buf := make([]byte, 8)

	for {
		n, addr, err := ServerConn.ReadFromUDP(buf)
		d := binary.BigEndian.Uint64(buf)
		fmt.Println(d)
		if err != nil {
			panic(err)
		}
		ServerConn.WriteToUDP(buf[0:n], addr)
	}
}
